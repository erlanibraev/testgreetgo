package com.greetgo.test.dao;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

/**
 * Создал Ибраев Ерлан 23.12.16.
 */
@Configuration
@MapperScan("com.greetgo.test.dao.repsitories")
public class DAOConfig {

}
