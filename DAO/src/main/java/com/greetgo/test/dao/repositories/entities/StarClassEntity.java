package com.greetgo.test.dao.repositories.entities;

/**
 * Создал Ибраев Ерлан 23.12.16.
 */
public class StarClassEntity {
    private Long id;
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
