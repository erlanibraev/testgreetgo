package com.greetgo.test.dao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Создал Ибраев Ерлан 23.12.16.
 */
@SpringBootApplication
public class GreetGoDAO {
    public static void main(String... args) {
        SpringApplication.run(GreetGoDAO.class, args);
    }
}
