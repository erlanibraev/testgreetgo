package com.greetgo.test.dao.repositories;

import com.greetgo.test.dao.repositories.entities.StarClassEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Создал Ибраев Ерлан 23.12.16.
 */
@Mapper
public interface IStarClassRepository  {

    @Select("SELECT * FROM starclass")
    List<StarClassEntity> findAll();
}
