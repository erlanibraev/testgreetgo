package com.greetgo.test.dao.repositories;

import com.greetgo.test.dao.repositories.entities.StarEntity;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * Создал Ибраев Ерлан 23.12.16.
 */
@Mapper
public interface IStarRepository {
    @Select("SELECT star.id, star.coordinate1, star.coordinate2, star.discoverer, star.starclass, starclass.name  FROM star LEFT JOIN starclass on starclass.id = star.starclass")
    public List<StarEntity> findAll();

    @Select("SELECT star.id, star.coordinate1, star.coordinate2, star.discoverer, star.starclass, starclass.name  FROM star LEFT JOIN starclass on starclass.id = star.starclass where star.id = #{id}")
    public StarEntity findOne(@Param("id") Long id);

    @Select("SELECT DISTINCT discoverer FROM star")
    public List<String> findAllDicoverer();

    @Insert("INSERT INTO star (coordinate1, coordinate2, discoverer, starclass) VALUES (#{item.coordinate1}, #{item.coordinate2}, #{item.discoverer}, #{item.starclass})")
    public void add(@Param("item") StarEntity item);

    @Update("UPDATE star SET coordinate1 = #{item.coordinate1} , coordinate2 = #{item.coordinate2}, discoverer = #{item.discoverer}, starclass = #{item.starclass} where id = #{item.id}")
    public void edit(@Param("item") StarEntity item);

    @Delete("DELETE FROM star WHERE id = #{id}")
    public void delete(@Param("id") Long id);
}
