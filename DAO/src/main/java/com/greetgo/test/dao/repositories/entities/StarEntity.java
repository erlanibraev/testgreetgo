package com.greetgo.test.dao.repositories.entities;

/**
 * Создал Ибраев Ерлан 23.12.16.
 */
public class StarEntity {
    private Long id;
    private String coordinate1;
    private String coordinate2;
    private String discoverer;
    private Long starclass;
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCoordinate1() {
        return coordinate1;
    }

    public void setCoordinate1(String coordinate1) {
        this.coordinate1 = coordinate1;
    }

    public String getCoordinate2() {
        return coordinate2;
    }

    public void setCoordinate2(String coordinate2) {
        this.coordinate2 = coordinate2;
    }

    public String getDiscoverer() {
        return discoverer;
    }

    public void setDiscoverer(String discoverer) {
        this.discoverer = discoverer;
    }

    public Long getStarclass() {
        return starclass;
    }

    public void setStarclass(Long starclass) {
        this.starclass = starclass;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
