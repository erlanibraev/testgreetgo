package com.greetgo.test.dao;

import com.greetgo.test.dao.repositories.IStarClassRepository;
import com.greetgo.test.dao.repositories.IStarRepository;
import com.greetgo.test.dao.repositories.entities.StarClassEntity;
import com.greetgo.test.dao.repositories.entities.StarEntity;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Создал Ибраев Ерлан 23.12.16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GreetGoDAOTest {

    @Autowired
    private IStarClassRepository starClassRepository;

    @Autowired
    private IStarRepository starRepository;


    @Test
    public void test01() {
        List<StarClassEntity> result = starClassRepository.findAll();
        printStarClasses(result);
        assertEquals(result.size(), 7);
    }

    @Test
    public void test02() {
        List<StarEntity> result = starRepository.findAll();
        printStars(result);
        assertEquals(result.size(), 14);
    }

    @Test
    public void test03() {
        List<String> result = starRepository.findAllDicoverer();
        result
                .forEach(s -> System.out.println(s));
        assertEquals(result.size(), 2);
    }

    @Test
    public void test04() {
        StarEntity entity = new StarEntity();
        entity.setCoordinate1("COORD 1");
        entity.setCoordinate2("COORD 2");
        entity.setDiscoverer("ИМЯРЕК 1");
        entity.setStarclass(6L);
        starRepository.add(entity);
        List<StarEntity> result = starRepository.findAll();
        printStars(result);
        assertEquals(result.size(),15);
    }

    @Test
    public void test05() {
        StarEntity entity = starRepository.findOne(1L);
        assertNotNull(entity);
        printStar(entity);
    }

    @Test
    public void test06() {
        StarEntity entity = starRepository.findOne(1L);
        assertNotNull(entity);
        entity.setDiscoverer("ИМЯРЕК 2");
        starRepository.edit(entity);
        StarEntity result = starRepository.findOne(1L);
        printStar(result);
        assertEquals(result.getDiscoverer(), entity.getDiscoverer());
    }

    @Test
    public  void test07() {
        StarEntity result = starRepository.findOne(10000L);
        assertNull(result);
    }

    @Test
    public void test08() {
        starRepository.delete(1L);
        StarEntity result = starRepository.findOne(1L);
        assertNull(result);
        printStars(starRepository.findAll());
    }

    private void printStarClasses(List<StarClassEntity> list) {
        list.forEach(starClass -> printStarClass(starClass));
    }

    private void printStarClass(StarClassEntity starClass) {
        System.out.print(starClass.getId());
        System.out.print(") ");
        System.out.print(starClass.getName());
        System.out.print("; ");
        System.out.println();
    }

    private void printStars(List<StarEntity> list) {
        list.forEach(star -> printStar(star));
    }

    private void printStar(StarEntity star) {
        System.out.print(star.getId());
        System.out.print(") ");
        System.out.print(star.getCoordinate1());
        System.out.print(" : ");
        System.out.print(star.getCoordinate2());
        System.out.print("; ");
        System.out.print(star.getStarclass());
        System.out.print("; ");
        System.out.print(star.getName());
        System.out.print("; ");
        System.out.print(star.getDiscoverer());
        System.out.println();
    }
}