Тестовое задание "GreetGo"

Модули:
1) DAO - работа с БД
Используется БД H2.
Схема и тестовые данные в файле DAO/src/main/resources/schema.sql;

2) WEB - веб-интерфейс
При сборке создаются два артефакта в WEB/target

WEB-1.0-SNAPSHOT.war - для деплоя на сервер приложений
WEB-1.0-SNAPSHOT-exec.war - исполняемый файл
Запуск:
java -jar WEB-1.0-SNAPSHOT-exec.war
Веб интерфейс будет доступен по адресу: http://localhost:8080

Пользователь    пароль
test           test

проект: https://bitbucket.org/erlanibraev/testgreetgo.git