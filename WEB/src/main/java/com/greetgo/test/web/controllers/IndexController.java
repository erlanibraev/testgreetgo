package com.greetgo.test.web.controllers;

import com.greetgo.test.dao.repositories.IStarClassRepository;
import com.greetgo.test.dao.repositories.IStarRepository;
import com.greetgo.test.dao.repositories.entities.StarClassEntity;
import com.greetgo.test.dao.repositories.entities.StarEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Создал Ибраев Ерлан 24.12.16.
 */
@Controller
public class IndexController {

    private long noDeleteStarClass;

    private IStarRepository starRepository;
    private IStarClassRepository starClassRepository;

    @RequestMapping(value=""
            , method = RequestMethod.GET
            , produces = MediaType.TEXT_HTML_VALUE + ";charset=utf-8")
    public String index(Model model) {
        model.addAttribute("stars", starRepository.findAll());
        model.addAttribute("noDeleteStarClass",noDeleteStarClass);
        return "index";
    }

    @RequestMapping(value="edit"
            , method = RequestMethod.GET
            , produces = MediaType.TEXT_HTML_VALUE + ";charset=utf-8")
    public String edit(@RequestParam(value = "id", required = false) Long id, Model model) {
        StarEntity star = id != null ? starRepository.findOne(id) : new StarEntity();
        model.addAttribute("starclasses", starClassRepository.findAll());
        model.addAttribute("star", star);
        model.addAttribute("discoverers", starRepository.findAllDicoverer());
        return "edit";
    }

    @RequestMapping(value="editform", method = RequestMethod.POST)
    public String editForm(@ModelAttribute("star") StarEntity star) {
        if (star.getId() != null) {
            starRepository.edit(star);
        } else {
            starRepository.add(star);
        }
        return "redirect:/";
    }

    @RequestMapping(value="delete"
            , method = RequestMethod.GET
            , produces = MediaType.TEXT_HTML_VALUE + ";charset=utf-8")
    public String delete(@RequestParam(value = "id", required = false) Long id, Model model) {
        StarEntity star = id != null ? starRepository.findOne(id) : null;
        if(star != null && !star.getStarclass().equals(noDeleteStarClass)) {
            starRepository.delete(id);
        }
        return "redirect:/";
    }

    @RequestMapping(value = "discoverer"
            , method = RequestMethod.GET
            , produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public List<String> discoverer() {
        return starRepository.findAllDicoverer();
    }

    @ModelAttribute("star")
    public StarEntity star() {
        return new StarEntity();
    }

    @ModelAttribute("starClasses")
    public List<StarClassEntity> starClasses() {
        return new ArrayList<>();
    }

    @Autowired
    public void setStarRepository(IStarRepository starRepository) {
        this.starRepository = starRepository;
    }

    @Autowired
    public void setStarClassRepository(IStarClassRepository starClassRepository) {
        this.starClassRepository = starClassRepository;
    }

    @Value("${noDeleteStarClass}")
    public void setNoDeleteStarClass(long noDeleteStarClass) {
        this.noDeleteStarClass = noDeleteStarClass;
    }
}
