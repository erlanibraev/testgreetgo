package com.greetgo.test.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.lang.reflect.Field;
import java.nio.charset.Charset;

/**
 * Создал Ибраев Ерлан 24.12.16.
 */
@SpringBootApplication
public class GreetGoWEB {

    public static void main(String... args) throws NoSuchFieldException, IllegalAccessException {

        SpringApplication.run(GreetGoWEB.class);
    }
}
