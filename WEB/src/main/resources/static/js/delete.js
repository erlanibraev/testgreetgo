/**
 * Created by mad on 25.12.2016.
 */

function deleteItem(id) {
    $("<div><div>")
        .appendTo('body')
        .html("Удалить?")
        .dialog({
            title: "Удаление",
            resizable: false,
            modal: true,
            buttons: {
                Yes: function() {
                    window.location="/delete?id="+id;
                },
                No: function() {
                    $(this).dialog("close");
                }
            },
            close: function(event, ui) {
                $(this).remove();
            }
        });


}
