<%--
  Created by IntelliJ IDEA.
  User: mad
  Date: 24.12.16
  Time: 11:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <title>Тестовое задание GreetGo</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="jquery/jquery-ui.css" rel="stylesheet">
    <script src="jquery/external/jquery/jquery.js">

    </script>
    <script src="bootstrap/js/bootstrap.js">

    </script>
    <script src="jquery/jquery-ui.js">

    </script>
    <script src="js/delete.js">

    </script>
</head>
<body>
    <h1>Звезды</h1>
    <a href="edit"><button>Добавить</button></a>
<table border="1">
    <tr>
        <th>Координаты</th>
        <th>Тип</th>
        <th>Кто открыл</th>
        <th>Действия</th>
    </tr>
    <c:forEach items="${stars}" var="star">
        <tr>
            <td>${star.coordinate1} - ${star.coordinate2}</td>
            <td>${star.name}</td>
            <td>${star.discoverer}</td>
            <td>
                <a href="edit?id=${star.id}"><button>Изменить</button></a>
                <button <c:if test="${star.starclass == noDeleteStarClass}">disabled</c:if> onclick="deleteItem(${star.id})">Удалить</button>
            </td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
