<%--
  Created by IntelliJ IDEA.
  User: mad
  Date: 24.12.16
  Time: 12:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Редактирование</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="jquery/jquery-ui.css" rel="stylesheet">
    <script src="jquery/external/jquery/jquery.js">

    </script>
    <script src="bootstrap/js/bootstrap.js">

    </script>
    <script src="jquery/jquery-ui.js">

    </script>

</head>
<body>
<h1>Звезда</h1>
    <c:form modelAttribute="star" method="post" action="editform">
        <div  class="form-group">
        <c:hidden path="id"/>
        <label for="coordinate1">Координаты:</label><c:input path="coordinate1"/> : <c:input path="coordinate2"/>
        </div>
<div  class="form-group">
        <label for="starclass">Тип звезды:</label><c:select path="starclass">
            <c:options items="${starclasses}" itemValue="id" itemLabel="name"/>
        </c:select>
</div>
<div  class="form-group">

<label for="discoverer">Открыл:</label><c:input path="discoverer"/>
</div>
        <hr>
        <input type="submit" value="Сохранить">
        <a href="/"><input type="button" value="Отмена"></a>

    </c:form>
<script>
    $(function(){
        $.getJSON("/discoverer", function(data) {
            console.log(data);
            $('#discoverer').autocomplete({
                source: data
            })
        })
    })
</script>
</body>
</html>
